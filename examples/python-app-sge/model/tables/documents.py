# -*- coding: utf-8 -*-

from model_helper import *

class Documents(Base):
	__tablename__ = "documents"

	id = Column(Integer, primary_key = True)
	filename = Column(String(512), nullable = False)
	created_at = Column(DateTime)
	readed_at = Column(DateTime)
	readed = Column(Boolean, default = False)

class DocumentManager():
	entity = None
	session = None

	def __init__(self, entity, session):
		self.entity = entity
		self.session = session

	def create(self, filename):
		e = self.entity(
			filename = filename,
			created_at = datetime.datetime.now(),
			)
		self.session.add(e)
		self.session.commit()
		self.session.close()
		return e

	def update(self, entity, filename, readed = False , readed_at = None):
		entity.filename = filename
		entity.readed = readed
		entity.readed_at = readed_at
		self.session.add(entity)
		self.session.commit()
		self.session.close()
		return entity

	def get_all(self):
		q = self.session.query(self.entity).all()
		return q

